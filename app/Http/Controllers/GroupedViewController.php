<?php

namespace WorkDoneRight\GroupByTool\Controllers;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;
use Laravel\Nova\Http\Requests\NovaRequest;

class GroupedViewController
{
    public function index(NovaRequest $request)
    {
        $model = $request->input('model');

        $selectColumns = $request->input('select_columns');

        $groupByColumns = $request->input('group_by');

        $selectColumns = array_map(function ($column) {
            return \DB::raw($column);
        }, $selectColumns);

        $whereConditions = $request->input('using_where');

        /** @var Builder $query */
        $query = app($model)->select($selectColumns)->groupBy($groupByColumns);

        foreach ($whereConditions as $column => $whereCondition) {
            $query->where($column, $whereCondition);
        }

        $data = $query->get();

        return response()->json([
            'columns' => array_map(function ($item) {
                return Str::upper(Str::title(str_replace('_', ' ', $item)));
            }, array_keys($data->toArray()[0])),
            'data'    => $data,
        ]);
    }
}
