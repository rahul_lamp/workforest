<?php

namespace App\Observers;

use App\Tracker;

class UserObserver
{
    /**
     * Handle the tracker "created" event.
     *
     * @param  \App\Tracker  $tracker
     * @return void
     */
    public function created(Tracker $tracker)
    {
        //
    }

    /**
     * Handle the tracker "updated" event.
     *
     * @param  \App\Tracker  $tracker
     * @return void
     */
    public function updated(Tracker $tracker)
    {
        //
    }

    /**
     * Handle the tracker "deleted" event.
     *
     * @param  \App\Tracker  $tracker
     * @return void
     */
    public function deleted(Tracker $tracker)
    {
        //
    }

    /**
     * Handle the tracker "restored" event.
     *
     * @param  \App\Tracker  $tracker
     * @return void
     */
    public function restored(Tracker $tracker)
    {
        //
    }

    /**
     * Handle the tracker "force deleted" event.
     *
     * @param  \App\Tracker  $tracker
     * @return void
     */
    public function forceDeleted(Tracker $tracker)
    {
        //
    }
}
