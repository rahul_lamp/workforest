Nova.booting((Vue, router, store) => {
  Vue.component('group_by_tool', require('./components/Tool'))
	if (process.env.NODE_ENV !== 'production') {
	    Vue.config.devtools = true
	}
})
