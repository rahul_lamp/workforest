<?php

namespace Workdoneright\GroupByTool;

use Laravel\Nova\ResourceTool;

class GroupByTool extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Group_By_Tool';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'group_by_tool';
    }
  
    public function setTitle($title)
    {
        $this->withMeta(['title' => $title]);

        return $this;
    }

    public function usingModel(string $model)
    {
        $this->withMeta(['model' => $model]);

        return $this;
    }

    public function groupBy($columns = [])
    {
        $this->withMeta(['group_by' => $columns]);

        return $this;
    }

    public function selectColumns($columns = [])
    {
        $this->withMeta(['select_columns' => $columns]);

        return $this;
    }

    public function usingWhereCondition($data)
    {
        $this->withMeta(['using_where' => $data]);

        return $this;
    }

	public function fields(Request $request)
	{ 
	   return [
	      // ... All your other fields here

	      GroupByTool::make()
		    ->usingModel(EventRegistration::class)
		    ->selectColumns([
		        "shirt_size",
		        "COUNT('shirt_size') as total_count",
		    ])
		    ->groupBy(['shirt_size'])
		    ->usingWhere([
		        'eventable_type' => \App\Models\StandaloneEvent::class,
		        'eventable_id'   => $this->id,
		    ])
		    ->setTitle('Count by T-Shirt Sizes'),
	     
	     ];
	}

}
