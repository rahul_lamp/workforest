<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::updateOrCreate(
            [
                'email' => 'tarun.narang@vinove.com'
            ],
            [
                'first_name' => 'Tarun',
                'last_name' =>' Narang',
                'employee_id' => 'V-5389',
                'name'  => 'Tarun Narang',
                'slug'  => 'tarun-narang',
                'email' => 'tarun.narang@vinove.com',
                'password' => bcrypt('12345678'),
                'phone' => '1234567891',
                'trial_ends_at' => Carbon::now()->addYear(1),
                'added_time' => Carbon::now(),
                'updated_time' => Carbon::now(),
            ]
        );

        User::updateOrCreate(
            [
                'email' => 'rahul.kumar3@mail.vinove.com'
            ],
            [
                'first_name' => 'Rahul',
                'last_name' =>' Kumar',
                'employee_id' => 'V-5348',
                'name'  => 'Rahul Kumar',
                'slug'  => 'rahul-kumar',
                'email' => 'rahul.kumar3@mail.vinove.com',
                'password' => bcrypt('12345678'),
                'phone' => '1234567891',
                'trial_ends_at' => Carbon::now()->addYear(1),
                'added_time' => Carbon::now(),
                'updated_time' => Carbon::now(),
            ]
        );
    }
}
